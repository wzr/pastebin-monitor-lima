import re

class RegExConfig():
    
    regexp = '\[\=(\w+)\=\]'  #[=CyberAttack=] 

    def read_config(self, filename):
        fp = open(filename, 'r')
        raw_config = fp.readlines()
        fp.close()
        return raw_config

    def load_config(self, raw_config):
        config = dict()
        psection = "__default__"
        
        for line in raw_config:
            result = self.is_key(line)
            if result and not result in config.keys():
                psection = result
                config[psection] = list()
            else:
                line = line.rstrip()
                if line:
                    config[psection].append(line)
        return config
                
    def is_key(self, line):
        match = re.search(self.regexp, line)
        return match.group(1) if match else None

    def parse(self, filename):
        raw_config  = self.read_config(filename)
        dict_config = self.load_config(raw_config)
        return dict_config
        
        
if __name__ == "__main__":
    rgp = RegExConfig()
    res = rgp.parse('/opt/pastebin_monitor/pastebin_monitor.keywords')
    for k,v in res.iteritems():
        print "%s - %s\n" % (k,v)
