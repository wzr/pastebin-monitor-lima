from cuisine import *
from fabric.contrib.files import upload_template
import os, jinja2

class PastebinMonitor():

    def __init__(self, configurations):
        self.core_host                      = configurations.get('General','host')
        self.pm_installation_dir            = configurations.get('Installation','pastebin_monitor_directory')
        self.pm_init_file                   = configurations.get('InitScripts','pastebin_monitor')
        self.pm_init_file_template          = configurations.get('Templates','pastebin_monitor_initscript')
        self.pm_configuration_file_template = configurations.get('Templates','pastebin_monitor_configuration')

        self.pastebin_monitor_user="pastebinmonitor"
        user_ensure(name=self.pastebin_monitor_user)

    def install(self):
        with mode_sudo(True):
            self.__dependencies()
            self.__package_install()
            self.stop()
            self.__update_config()
            self.__update_init_script()

    def update(self):
        upstart_stop('pastebin_monitor')
        print "update now..."
        upstart_ensure('pastebin_monitor')

    def start(self):
        upstart_ensure('pastebin_monitor')

    def stop(self):
        upstart_stop('pastebin_monitor')

    def __dependencies(self):
        package_ensure('python-dateutil')        

    def __package_install(self):
        dir_ensure(self.pm_installation_dir, recursive=True, mode="755", owner=self.pastebin_monitor_user, group=self.pastebin_monitor_user)
        file_upload(self.pm_installation_dir, "bin/pastebin_monitor.py", sudo=True)
        pm_binary = os.path.join(self.pm_installation_dir, "pastebin_monitor.py")
        file_ensure(pm_binary, mode="755", owner=self.pastebin_monitor_user, group=self.pastebin_monitor_user)
        file_upload(self.pm_installation_dir, "bin/regexconfig.py", sudo=True)
        pm_library = os.path.join(self.pm_installation_dir, "regexconfig.py")
        file_ensure(pm_library, mode="755", owner=self.pastebin_monitor_user, group=self.pastebin_monitor_user)

    def __update_config(self):
        fptemplate = open(self.pm_configuration_file_template, 'r')
        template_content = fptemplate.read()
        pm_config = os.path.join(self.pm_installation_dir, "pastebin_monitor.keywords")
        file_write(pm_config, template_content)
        file_ensure(pm_config, mode="755", owner=self.pastebin_monitor_user, group=self.pastebin_monitor_user)

    def __update_init_script(self):
        context = {
            "install_path": self.pm_installation_dir,
            "user": self.pastebin_monitor_user,
        }
        fptemplate = open(self.pm_init_file_template, 'r')
        template_content = fptemplate.read()
        config_content   = jinja2.Environment().from_string(template_content).render(context)
        file_write(self.pm_init_file, config_content)
        file_ensure(self.pm_init_file, mode="755", owner="root", group="root")
        sudo("update-rc.d -f pastebin_monitor defaults")
        sudo("update-rc.d -f pastebin_monitor enable")
