from cuisine import *
from fabric.contrib.files import sed

class Kibana():

    def __init__(self, configurations):
        self.core_packages_dir = configurations.get('Packages','directory')
        self.core_host = configurations.get('General','host')
        self.kb_package_url = configurations.get('Packages','kibana')
        self.kb_configuration_file = configurations.get('Configurations','kibana')
        self.kb_installation_dir = configurations.get('Installation','kibana_directory')

    def install(self):
        with mode_sudo(True):
            self.__dependencies()
            self.__package_install()
            self.__update_config()
            self.stop()

    def update(self):
        sudo("/etc/init.d/apache2 stop", pty=False)
        print "update now..."
        sudo("/etc/init.d/apache2 start", pty=False)

    def start(self):
        sudo("/etc/init.d/apache2 start", pty=False)
        import time
        time.sleep(3)

    def stop(self):
        sudo("/etc/init.d/apache2 stop", pty=False)

    def __dependencies(self):
        package_ensure('wget')
        package_ensure('unzip')
        package_ensure('apache2')
        package_ensure('php5')
        package_ensure('libapache2-mod-php5')

    def __package_install(self):
        dir_ensure(self.core_packages_dir, recursive=True)
        sudo("wget '%s' -O %s/kibana.zip" % (self.kb_package_url, self.core_packages_dir))
        sudo("unzip %s/kibana.zip -d %s" % (self.core_packages_dir, self.core_packages_dir))
        sudo("mv %s/kibana-*/* %s" % (self.core_packages_dir, self.kb_installation_dir))

    def __update_config(self):
        sed(self.kb_configuration_file, '"+window.location.hostname+"', '%s' % self.core_host, use_sudo=True, backup='')
