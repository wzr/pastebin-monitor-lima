from cuisine import *
from fabric.contrib.files import sed
import jinja2

class ElasticSearch():

    def __init__(self, configurations):
        self.core_packages_dir = configurations.get('Packages','directory')
        self.core_host = configurations.get('General','host')
        self.es_package_url = configurations.get('Packages','elasticsearch')
        self.es_config_file = configurations.get('Configurations','elasticsearch')
        self.es_config_file_template = configurations.get('Templates','elasticsearch_configuration')
        self.es_init_file = configurations.get('InitScripts','elasticsearch')
        self.es_mapping_file_template = configurations.get('Templates','elasticsearch_mapping')
        self.es_heap_size   = configurations.get('Specifications','elasticsearch_heap_size')
        self.es_cluster_name   = configurations.get('Specifications','elasticsearch_cluster_name')
        self.es_index_name   = configurations.get('Specifications','elasticsearch_index_name')

    def install(self):
        with mode_sudo(True):
            self.__dependencies()
            self.__package_install()
            self.stop()
            self.__update_config()
            self.__update_init_script()
            self.start()
            self.__mapping_install()
            self.stop()

    def update(self):
        sudo("/etc/init.d/elasticsearch stop", pty=False)
        print "update now..."
        sudo("/etc/init.d/elasticsearch start", pty=False)

    def start(self):
        sudo("/etc/init.d/elasticsearch start", pty=False)
        import time
        time.sleep(30)

    def stop(self):
        import time
        time.sleep(3)
        sudo("/etc/init.d/elasticsearch stop", pty=False)

    def __dependencies(self):
        package_ensure('openjdk-7-jre')
        package_ensure("curl")

    def __package_install(self):
        dir_ensure(self.core_packages_dir, recursive=True)
        sudo("wget '%s' -O %s/elasticsearch.deb" % (self.es_package_url, self.core_packages_dir))
        sudo("dpkg -i %s/elasticsearch.deb" % self.core_packages_dir)

    def __update_config(self):
        context = {
            "cluster_name": self.es_cluster_name,
        }
        fptemplate = open(self.es_config_file_template, 'r')
        template_content = fptemplate.read()
        config_content   = jinja2.Environment().from_string(template_content).render(context)
        file_write(self.es_config_file, config_content)
        file_ensure(self.es_config_file, mode="744", owner="root", group="root")

    def __update_init_script(self):
        sed(self.es_init_file, '#ES_HEAP_SIZE=2g', 'ES_HEAP_SIZE=%s' % self.es_heap_size, use_sudo=True, backup='')

    def __mapping_install(self):
        context = {
            "index_name": self.es_index_name,
        }
        fptemplate = open(self.es_mapping_file_template, 'r')
        template_content = fptemplate.read()
        mapping_content   = jinja2.Environment().from_string(template_content).render(context)
        sudo("curl -XPUT %s:9200/_template/%s -d '%s'" % (self.core_host, self.es_index_name, mapping_content))
